@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <div class="table-responsive">
                        <form>
                            <div class="form-group">
                                <label>Usuario</label>
                                <select class="form-control select2" id="user">
                                    <option value=""></option>
                                    @foreach(\App\User::where('role_id', 2)->get() as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group ">
                                <label for="range">Rango:</label>
                                    <select class="form-control select2" id="range">
                                        <option value=""></option>
                                        <option value="1">Hoy</option>
                                        <option value="2">Ayer</option>
                                        <option value="3" selected="">&Uacute;ltimos 7 d&iacute;as</option>
                                        <option value="4">Este mes</option>
                                        <option value="5">Mes pasado</option>
                                        <option value="6">Este a&ntilde;o</option>
                                        <option value="7">A&ntilde;o pasado</option>
                                        <option value="8">Personalizado</option>
                                    </select>
                            </div>

                                <div class="form-group">
                                    <!-- <div class="bg-gradient p-1 text-white">01/03/2020 - 26/003/2020</div> -->
                                    <input type="text" name="dates" id="dates" class="form-control" style="" readonly="">
                                </div>

                        </form>
                        
                        <div id="result" class="row">
                        
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" >

<style>
    
    .dataTables_wrapper .dataTables_filter {
       display:none;
    }
    .dataTables_length{
       display:none; 
    }
    
    .dataTables_info{
        display:none; 
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #fff !important;
        border: 1px solid #979797;
        background-color: white;
        background: rgb(47, 74, 159);
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        box-sizing: border-box;
        display: inline-block;
        min-width: 1.5em;
        padding: 0.5em 1em;
        margin-left: 2px;
        text-align: center;
        text-decoration: none !important;
        cursor: pointer;
        *cursor: hand;
        color: #333 !important;
        border: 1px solid transparent;
        border-radius: 2px;
    }
    
    
    div.dataTables_wrapper div.dataTables_paginate {
        margin: 0;
        white-space: nowrap;
        text-align: right;
    }
</style>
@stop

@section('javascript')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            
            $('input[name="dates"]').daterangepicker({
                locale: {
                format: 'DD-MM-YYYY'
                },
                maxDate: '{{date('d-m-Y')}}'
            });
            
             function range(){
                var range = $("#range").val();
                $("#dates").prop("readonly", true);
                $("#dates").prop("disabled", true);
                
                 switch(range){
                    case "1":
                      $("#dates").val("{{date('d-m-Y')}} - {{date('d-m-Y')}}");
                    break;
                    case "2":
                      $("#dates").val("{{ date('d-m-Y', strtotime(date('d-m-Y').' -1 day')) }} - {{date('d-m-Y', strtotime(date('d-m-Y').' -1 day'))}}");
                    break;
                    case "3":
                      $("#dates").val("{{ date('d-m-Y', strtotime(date('d-m-Y').' -7 days')) }} - {{date('d-m-Y')}}");
                    break;
                    case "4":
                      $("#dates").val("{{ date('01-m-Y') }} - {{date('d-m-Y')}}");
                    break;
                    case "5":
                        @php
                          $lastMonth = date('m', strtotime('-1 month'));
                          $lastDay = date('t', strtotime('-1 month'));
                        @endphp
                      $("#dates").val("{{ date('01-'.$lastMonth.'-Y')}} - {{date($lastDay.'-'.$lastMonth.'-Y')}}");
                    break;
                    case "6":
                      $("#dates").val("{{ date('01-01-Y') }} - {{date('d-m-Y')}}");
                    break;
                    case "7":
                        @php
                          $lastYear = date('Y', strtotime('-1 year'));
                        @endphp
                      $("#dates").val("{{ date('01-01-'.$lastYear)}} - {{date('31-12-'.$lastYear)}}");
                    break;
                    case "8":
                      $("#dates").val("{{date('01-m-Y')}} - {{date('d-m-Y')}}");
                      $("#dates").prop("disabled", false);
                    break; 
                }
                    
                getData();
            }
            
            function getData(){
                var range = $("#range").val();
                var dates = $("#dates").val();
                var user = $("#user").val();
                
                if(range == "" || dates == ""){
                    return;
                }
                
                $.get("{{route('estadisticasData.admin')}}", {range:range, dates:dates, user:user}, function(d){
                    $("#result").html(d);
                })
            }
            
            $("#range").change(function(){ range(); });
            $("#user").change(function(){ range(); });
            
            range();
            
            $("#dates").change(function(){
                getData();
            });
            
        });
    </script>
@stop
