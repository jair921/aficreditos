@extends('layout.master')

 @php
  $months = [ 1=>'Enero', 2=>'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'];
  $l = [];
  for($i=1; $i<=(int)date("d"); $i++){
      $c = \App\Statics::where('user_id', auth()->id())->where('type', 1)
      ->where('created_at', '>=', date('Y-m-'.$i.' 00:00:00'))
      ->where('created_at', '<=', date('Y-m-'.$i.' 23:59:59'))->count();
      $l[] = '["'.$i.'", '.$c.', "#2F4A9F"]';
  }
@endphp

@section('content')
<main id="main" role="main">

    <section class=" text-center mb-5">
        <div class="container">
            <div class="row w-100 justify-content-md-center text-white">   
                <div class="col text-center bg-gradient-radius p-4 m-3">
                    <p>Tu comisi&oacute;n actual es:</p>
                    <h2 class="font-weight-bold">{{auth()->user()->commission}}%</h2>
                </div> 
                <div class="col text-center bg-gradient-radius p-4 m-3">
                    <p>Tu nivel actual es:</p>
                    @php
                       $level = \App\Level::find(auth()->user()->level_id);
                       $levelName = 'Principiante';
                       if($level){
                           $levelName = $level->name;
                       }
                    @endphp
                    <h2 class="font-weight-bold">{{ $levelName }}</h2>
                </div> 
                <div class="col text-center bg-gradient-radius p-4 m-3">
                    <p>Generado este mes</p>
                    <h2 class="font-weight-bold">${{number_format($payMonth, 2)}}</h2>
                </div> 
                <div class="col text-center bg-azul-radius p-4 m-3">
                    <p>Generado hoy</p>
                    <h2 class="font-weight-bold" >${{number_format($payToday, 2)}}</h2>
                </div> 

            </div>
        </div>

    </section>
    <section class=" text-center mb-5">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-8">
                    <div class="row justify-content-md-center border p-5">
                        <div class="col-md-12 text-center pb-3 text-azul">
                            <h5>Estad&iacute;stica de hoy</h5>
                        </div>   
                        <div class="col text-center p-2 m-2">
                            <p>Clics:<span class="font-weight-bold"> {{$clicks}}</span></p>
                        </div> 
                        <div class="col text-center  p-2 m-2">
                            <p>Impresioness:<span class="font-weight-bold"> {{$prints}}</span></p>
                        </div> 
                        <div class="col text-center  p-2 m-2">
                            <p>Registros:<span class="font-weight-bold"> {{$registers}}</span></p>
                        </div> 
                        <div class="col text-center  p-2 m-2">
                            <p>Conversiones:<span class="font-weight-bold"> {{$pays}}</span></p>
                        </div> 

                    </div>
                </div>
                <div class="col"></div>
            </div>

        </div>

    </section>
    <section>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-12 text-left pb-3 text-azul">
                    <h5>Estad&iacute;sticas {{$months[(int)date('m')]}}</h5>
                </div>
                <div class="col-md-12 ">
                    <div id="chart_wrap">
                        <div id="chart_div"></div>
                    </div>

                </div>

            </div>
        </div>

    </section>
</main>
@endsection



@section('js')
 <script type="text/javascript">
            google.charts.load("current", {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    
                    ["Element", "Clicks", {role: "style"}],
                    {!!implode(',', $l)!!}
                ]);

                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"},
                    2]);

                var options = {
                    title: "",
                    width: '100%',
                    height: '700px',
                    bar: {groupWidth: "50%"},
                    legend: {position: "none"},
                };
                var chart = new google.visualization.ColumnChart(document.getElementById("chart_div"));
                chart.draw(view, options);
            }
        </script>
@endsection