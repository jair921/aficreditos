@extends('layout.master')

@section('content')
<main id="main" role="main">
    <section class="bg-white text-center">
        <div class="container">
            <form class="bg-form bg-gradient">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center text-white pb-3">
                        <p>Material de Marketing</p>
                    </div>

                    <div class="col-md-6 ">

                        <div class="form-group row ">
                            <label for="site" class="col-sm-4 col-form-label text-white text-right ">Marca:</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-lg p-2" name="site" id="site">
                                    @foreach(\App\Site::where('status', 1)->get() as $site)
                                        <option value="{{$site->id}}">{{$site->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label for="material" class="col-sm-4 col-form-label text-white text-right ">Tipo de Creatividad:</label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-lg p-2" name="material" id="material">
                                    @foreach(\App\MaterialType::all() as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div></div>
                <!-- <button type="submit" class="btn btn-form-g">Guardar</button> -->
            </form>
        </div>
    </section>


    <section class="bg-white">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <p>Importante! si queres trackear tus estadisticas, tenes que incluir el parametro  “id” en el enlace correspondiente.</p>
                    <p>Ejemplo: http://www.123efectivo.com/ref/?cff={{auth()->id()}}&id=”track1”</p>
                </div>
            </div>
        </div>
    </section>
    <div id="creatividad">
    </div>
</main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            
            function loadMaterial(){
                var site = $("#site").val();
                var material = $("#material").val();
                if(!site.length || !material.length){
                    $("#creatividad").html("");
                    return;
                }
                
                $.get("{{route('home')}}/material/"+material+"/"+site, {}, function(d){
                    $("#creatividad").html(d);
                });
                
            }
            
            loadMaterial();
            
            $("#material").change(function(){ loadMaterial(); });
        });
        
        function copy(e) {
            /* Get the text field */
            var copyText = document.getElementById(e);

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            //alert("Copied the text: " + copyText.value);
        } 
    </script>
@endsection
