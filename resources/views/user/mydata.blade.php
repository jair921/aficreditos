@extends('layout.master')

@section('content')

<main id="main" role="main">
    <section class="bg-white text-center mb-5">
        <div class="container">


            <div class="row">   
                <div class="col-md-6 border-col" >
                    <form class="bg-light" action="{{route('changeData')}}" method="post">
                        @csrf
                        <div class="text-center text-naranja pb-5 pt-5">
                            <h3>Datos Personales</h3>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="name" class="col-sm-2 col-form-label text-azul">Nombre:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" value="{{auth()->user()->name}}" required="">
                            </div>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="phone" class="col-sm-2 col-form-label text-azul">Tel&eacute;fono:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="phone" name="phone" value="{{auth()->user()->phone}}" required="">
                            </div>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="address" class="col-sm-2 col-form-label text-azul">Domicilio:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="address" name="address" value="{{auth()->user()->address}}" required="">
                            </div>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="provincia" class="col-sm-2 col-form-label text-azul">Provincia:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="provincia" name="provincia" style="padding: 0rem 1rem; height: 3.5em;">
                                    <option value=""></option>
                                    @foreach(\App\Provincia::all() as $provincia)
                                        <option value="{{$provincia->id}}" {!!(auth()->user()->provincia == $provincia->id ) ? 'selected=""' : ''!!}>{{$provincia->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <button type="submit" class="btn btn-form-a">Enviar</button> 

                        </div>
                    </form>
                </div>

                <div class="col-md-6" >
                    <form method="post" action="{{route('changePass')}}">
                        @csrf
                        <div class="text-center text-naranja pb-5 pt-5">
                            <h3>Contraseña</h3>
                        </div>

                        <div class="form-group row pl-5 pr-5">
                            <label for="password" class="col-sm-2 col-form-label text-azul">Actual:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="password" required="" name="password">
                            </div>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="newpassword" class="col-sm-2 col-form-label text-azul">Nueva:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="newpassword" name="newpassword" required="">
                            </div>
                        </div>
                        <div class="form-group row pl-5 pr-5">
                            <label for="newpasswordconfirm" class="col-sm-2 col-form-label text-azul">Repetir Nueva:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="newpasswordconfirm" name="newpassword_confirmation" required="">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <button type="submit" class="btn btn-form-a">Enviar</button> 

                        </div>
                    </form>
                </div>
            </div>


        </div>
    </section>
</main>
@endsection