@extends('layout.master')

@section('css')
<style>
    #amount-error{
       width: 100%;
    }
</style>
@endsection

@section('content')

<main id="main">
    <section class="bg-white">
        <div class="container">

            <div class="section-title pb-5">
                <h2>Datos de Facturaci&oacute;n</h2>
                <p>Introduce a continuacion los datos de tu cuenta bancaria, 
                    los cuales usaremos para transferirte tus ganancias mensualmente.</p>
            </div>

            <form class="bg-form" method="post" action="{{route('changeDataBank')}}">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-white">Nombre y apellido:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" value="{{auth()->user()->name}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cuit" class="col-sm-4 col-form-label text-white">Cuit/Cuil:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cuit" name="cuit" value="{{auth()->user()->cuit}}" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="type" class="col-sm-4 col-form-label text-white">Tipo de cuenta:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="type" name="type" required="" style="padding: 0rem 1rem; height: 3.5em;">
                                    <option value=""></option>
                                    @foreach(\App\TypesAccount::all() as $type)
                                    <option value="{{$type->id}}" {{($type->id == auth()->user()->type)?'selected':''}}>{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="bank" class="col-sm-4 col-form-label text-white">Banco:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="bank" name="bank" required="" style="padding: 0rem 1rem; height: 3.5em;">
                                    <option value=""></option>
                                    @foreach(\App\Bank::all() as $bank)
                                    <option value="{{$bank->id}}" {{($bank->id == auth()->user()->bank)?'selected':''}}>{{$bank->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cbu" class="col-sm-4 col-form-label text-white">No de CBU:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cbu" name="cbu" value="{{auth()->user()->cbu}}" required="" minlength="22" maxlength="22">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center pt-5">
                            <button type="submit" class="btn btn-form-fc">ENVIAR</button> 

                        </div>
                    </div>
                </div>

            </form> 
        </div>
    </section>

    <section class="bg-white">
        <div class="container">
            <div class="section-title-naranja">
                <h2 style="font-size: 38px" >Solicitar retiro</h2 >
            </div>

            <form class="bg-form" method="post" action="{{route('retiro')}}" id="frm">
                @csrf
                <div class="row">
                    <div class="col-md-12 text-center text-white">
                        <h5 class="p-0 m-0 ">Comisi&oacute;n: <span class="text-azu">${{ number_format((float)$realPay, 2) }}</span></h5>
                        <h5 class="p-0 m-0 ">Monto m&iacute;nimo: <span class="text-azu">${{number_format((float)$min, 2)}}</span></h5>
                    </div>
                    @if(!$ds)
                        <div class="col-md-12 text-center text-white">
                             <lavel>Monto a retirar</lavel>
                             <input type="text" name="amount" id="amount" required>
                        </div>
                    @endif
                    <div class="col-md-12 text-center">
                        <div class="form-group row justify-content-center pt-5">
                            <button type="submit" class="btn btn-form-fc" @if($ds) disabled="" @endif>Retirar</button> 
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </section>
</main>

<section class="bg-white">
    <div class="container">
        <div class="section-title-naranja">
            <h2 style="font-size: 38px" >Historial de Pagos</h2 >
        </div>

        <div class="table-responsive" style="margin-bottom: 50px;">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Monto</th>
                        <th>CBU</th>
                        <th>Estado</th>

                    </tr>
                </thead>
                <tbody>
                    @if(count($withdrawals))
                     @foreach($withdrawals as $withdrawal)
                     <tr>
                         <td>{{$withdrawal->created_at}}</td>
                         <td>{{ number_format((float)$withdrawal->amount, 2) }}</td>
                         <td>{{$withdrawal->cbu}}</td>
                         <td>{{$withdrawal->status=='2'?'Pagado':'Pendiente'}}</td>
                     </tr>
                     @endforeach
                    @endif
                    @if(!count($withdrawals))
                     <tr>
                         <td colspan="4">Sin datos</td>
                     </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

</section>
</main>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/localization/messages_es.js"></script>
<script>
    $(document).ready(function(){
        if($("#amount").length){
            $("#frm").validate({
                rules:{
                    amount:{required:true, number:true, max:{{$realPay}}, min:{{$min}} }
                }
            });
        }
    });
</script>
@endsection