@extends('layout.master')

@section('content')
<main id="main" style="margin-bottom: 10rem;">
    <section class="bg-white">
        <div class="container">

            <div class="section-title pb-5">
                <h2>Formulario de Contacto</h2>
                <p></p>
            </div>

            <form class="bg-form p-5" method="post" action="{{route('contactoPost')}}">
                @csrf
                <div class="row">   
                    <div class="col-md-12" >
                        <div class="form-group row ">
                            <label for="asunto" class="col-sm-2 col-form-label text-white text-right ">Asunto:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="asunto" name="asunto" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mensaje" class="col-sm-2 col-form-label text-white text-right">Mensaje:</label>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" id="mensaje" row="10" name="mensaje" required=""></textarea>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center pt-5">
                            <button type="submit" class="btn btn-form-fc">ENVIAR</button> 

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

</main>
@endsection