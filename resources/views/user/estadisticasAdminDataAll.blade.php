@php
   if($range != "8"){
    $periods = new \DatePeriod(
             new \DateTime( date('Y-m-d 00:00:00', strtotime($dateStart))),
             new \DateInterval($di),
             new \DateTime( date('Y-m-d 23:59:59', strtotime($dateEnd)))
        );
    }
    $dateStartA = date('Y-m-d 00:00:00', strtotime($dateStart));
    $dateEndA = date('Y-m-d 23:59:59', strtotime($dateEnd));
    if($range == "8"){
        $dateStartO = date('d-m-Y', strtotime($dateStart));
        $dateEndO = date('d-m-Y', strtotime($dateEnd));
        $dateStart = date('Y-m-d 00:00:00', strtotime($dateStart));
        $dateEnd = date('Y-m-d 23:59:59', strtotime($dateEnd));
    }
@endphp
<section class="bg-white">
	<div class="container">
              <div class="table-responsive" style="margin-bottom: 50px;">
                <table class="table table-bordered text-white" id="dataTable1" width="100%" cellspacing="0" style="background: #2F4A9F;">
                  <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Afiliado</th>
                      <th>ID Track</th>
                      <th>Marca</th>
                      <th>Clicks</th>
                      <th>Impresiones</th>
                      <th>Registros</th>
                      <th>Conversiones</th>
                      <th>Comisi&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody>
                     
                      
                      @if($range != "8")
                           @foreach($periods as $period)
                                 @foreach(\App\Site::where('status', 1)->get() as $site)
                                     @php
                                         $tracks = \App\Statics::selectRaw('distinct track')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                         $users = \App\Statics::selectRaw('distinct user_id')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                      @endphp
                                      @foreach($users as $user)
                                    @foreach($tracks as $track)
                                        @php
                                            switch($range){
                                               case "1":
                                               case "2":
                                                case "3":
                                                case "4":
                                                case "5":
                                                    $format = 'd-m-Y';
                                                    $dateStart = $period->format('Y-m-d 00:00:00');
                                                    $dateEnd = $period->format('Y-m-d 23:59:59');
                                               break;
                                               
                                               case "6":
                                               case "7":
                                                    $format = 'm-Y';
                                                    $dateStart = $period->format('Y-m-01 00:00:00');
                                                    $dateEnd = $period->format('Y-m-t 23:59:59');
                                               break;
                                               
                                               case "9":
                                                    $format = 'Y';
                                                    $dateStart = $period->format('Y-01-01 00:00:00');
                                                    $dateEnd = $period->format('Y-12-31 23:59:59');
                                               break;
                                            }
                                            
                                            
                                            
                                            $clicks = \App\Statics::where('type', 1)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $prints = \App\Statics::where('type', 4)->where('user_id', $user->user_id)
                                                ->where('site', $site->url)
                                                ->where('track', $track->track)
                                                ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $registers = \App\Statics::where('type', 2)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $pays = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $commissions = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
                                                 $sumCommissions = 0;

                                            foreach($commissions as $cm){
                                                $sumCommissions += ($cm->mount * ($cm->commission/100));
                                            }
                                        @endphp
                                        <tr>
                                          <td>{{$period->format($format)}}</td>
                                          <td>{{$user->user->name}} - {{$user->user->username}}</td>
                                          <td>{{$track->track}}</td>
                                          <td>{{$site->name}}</td>
                                          <td>{{$clicks}}</td>
                                          <td>{{$prints}}</td>
                                          <td>{{$registers}}</td>
                                          <td>{{$pays}}</td>
                                          <td>$ {{ number_format($sumCommissions, 2) }}</td>
                                        </tr>
                                    @endforeach
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endif
                        
                        @if($range == "8")
                                @foreach(\App\Site::where('status', 1)->get() as $site)
                                    @php
                                         $tracks = \App\Statics::selectRaw('distinct track')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                          $users = \App\Statics::selectRaw('distinct user_id')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                      @endphp
                                      @foreach($users as $user)
                                        @foreach($tracks as $track)
                                        @php
    
                                            $clicks = \App\Statics::where('type', 1)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $prints = \App\Statics::where('type', 4)->where('user_id', $user->user_id)
                                                ->where('site', $site->url)
                                                ->where('track', $track->track)
                                                ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $registers = \App\Statics::where('type', 2)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $pays = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
                                            $commissions = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                                 ->where('site', $site->url)
                                                 ->where('track', $track->track)
                                                 ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
                                                 $sumCommissions = 0;
                                            foreach($commissions as $cm){
                                                $sumCommissions += ($cm->mount * ($cm->commission/100));
                                            }
                                        @endphp
                                        <tr>
                                          <td>{{$dateStartO}} - {{$dateEndO}}</td>
                                          <td>{{$user->user->name}} - {{$user->user->username}}</td>
                                          <td>{{$track->track}}</td>
                                          <td>{{$site->name}}</td>
                                          <td>{{$clicks}}</td>
                                          <td>{{$prints}}</td>
                                          <td>{{$registers}}</td>
                                          <td>{{$pays}}</td>
                                          <td>$ {{ number_format($sumCommissions, 2) }}</td>
                                        </tr>
                                    @endforeach
                                    @endforeach
                                @endforeach
                        @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
	</div>
</section>
<script>
    $(document).ready( function () {
        $('#dataTable1').DataTable({
            pagingType: 'numbers',
            "order": [[ 0, "desc" ]]
        });
    } );
</script>
<section class="bg-white">
  <div class="container">
        <div class="section-title">
          <h2 style="font-size: 38px">Conversiones</h2>
        </div>
             <div class="col">
              <div class="table-responsive" style="margin-bottom: 50px;">
                <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Afiliado</th>
                      <th>ID Track</th>
                      <th>Marca</th>
                      <th>Nombre cliente</th>
                      <th>Comisiones</th>
                    
                    </tr>
                  </thead>
                  <tbody>
                      @if($range != "8")
                           @foreach($periods as $period)
                                 @foreach(\App\Site::where('status', 1)->get() as $site)
                                 @php
                                         $tracks = \App\Statics::selectRaw('distinct track')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                     $users = \App\Statics::selectRaw('distinct user_id')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                      @endphp
                                      @foreach($users as $user)
                                        @foreach($tracks as $track)
                                    @php
                                        switch($range){
                                           case "1":
                                           case "2":
                                            case "3":
                                            case "4":
                                            case "5":
                                                $format = 'd-m-Y';
                                                $dateStart = $period->format('Y-m-d 00:00:00');
                                                $dateEnd = $period->format('Y-m-d 23:59:59');
                                           break;
                                           
                                           case "6":
                                           case "7":
                                                $format = 'm-Y';
                                                $dateStart = $period->format('Y-m-01 00:00:00');
                                                $dateEnd = $period->format('Y-m-t 23:59:59');
                                           break;
                                           
                                           case "9":
                                                $format = 'Y';
                                                $dateStart = $period->format('Y-01-01 00:00:00');
                                                $dateEnd = $period->format('Y-12-31 23:59:59');
                                           break;
                                        }

                                        $commissions = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                             ->where('site', $site->url)
                                              ->where('track', $track->track)
                                             ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
                                    @endphp
                                         @foreach($commissions as $commission)
                                        <tr>
                                          <td>{{$period->format($format)}}</td>
                                          <td>{{$user->user->name}} - {{$user->user->username}}</td>
                                           <td>{{$track->track}}</td>
                                          <td>{{$site->name}}</td>
                                          <td>{{$commission->name}}</td>
                                          <td>$ {{ number_format($commission->mount * ($commission->commission/100)) }}</td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                    @endforeach
                            @endforeach
                            @endforeach
                        @endif
                        
                         @if($range == "8")
                                @foreach(\App\Site::where('status', 1)->get() as $site)
                                @php
                                         $tracks = \App\Statics::selectRaw('distinct track')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                      $users = \App\Statics::selectRaw('distinct user_id')
                                                             ->where('site', $site->url)
                                                             ->where('created_at', '>=', $dateStartA)
                                                             ->where('created_at', '<=', $dateEndA)->get();
                                      @endphp
                                      @foreach($users as $user)
                                @foreach($tracks as $track)
                                    @php
                                        $commissions = \App\Statics::where('type', 3)->where('user_id', $user->user_id)
                                             ->where('site', $site->url)
                                             ->where('track', $track->track)
                                             ->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
                                    @endphp
                                     @foreach($commissions as $commission)
                                        <tr>
                                          <td>{{$dateStartO}} - {{$dateEndO}}</td>
                                          <td>{{$user->user->name}} - {{$user->user->username}}</td>
                                          <td>{{$track->track}}</td>
                                          <td>{{$site->name}}</td>
                                          <td>{{$commission->name}}</td>
                                          <td>$ {{ number_format($commission->mount * ($commission->commission/100)) }}</td>
                                        </tr>
                                        @endforeach
                                @endforeach
                                @endforeach
                                @endforeach
                        @endif
                  </tbody>
                </table>
              </div>
              </div>
            </div>
          </div>

        </div>
  </div>
</section>
<script>
    $(document).ready( function () {
        $('#dataTable2').DataTable({
            pagingType: 'numbers',
            "order": [[ 0, "desc" ]]
        });
    } );
</script>