@foreach($materials as $material)

@php
    $LINKUSER = $link = $site->url.'/ref/?cff='.auth()->user()->id.'';
        
    $html = str_replace('LINKUSER', $link, $material->code);
    $html = str_replace('__site__', $material->site_id, $html);
    $html = str_replace('__user__', auth()->id(), $html);
    
    $linkMedia = $material->code = $html;
@endphp

@switch($material->type)
    @case(1)
    @case(4)
      <section class="text-center" >
        <div class="container bg-white">
            <div class="row">
                <div class="col"></div>
                <div class="col-10">
                    <form class="bg-form">
                        <div class="row">
                            <div class="col-md-12 text-center text-white pb-3">
                                <h5>{{$material->name}}</h5>
                            </div>
                            <div class="col-md-9 text-white p-1">
                                <input type="text" class="form-control" value="{{$material->code}}" readonly="" id="link"></div>
                            <div class="col-md-3 pt-1">
                                <button type="submit" class="btn btn-form-g pl-5 pr-5" onclick="copy('link'); return false;">Copiar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col"></div>

            </div>
        </div>
    </section>
    @break
   
    
    @default
    @php
      switch($material->type){
       case 2;
         $code = view('user.materials.banner', compact('linkMedia', 'LINKUSER'));
       break;
       case 3;
         $code = view('user.materials.video', compact('linkMedia', 'LINKUSER'));
       break;
       case 5;
         $code = view('user.materials.popups', compact('linkMedia', 'LINKUSER'));
       break;
      }
    @endphp
        <section class="text-center" >
            <div class="container bg-black">

                <div class="row">
                    <div class="col"></div>
                    <div class="col-10">
                        <form class="bg-form bg-black">
                            <div class="row">
                                <div class="col-md-12 text-center text-white pb-3">
                                    <h5>{{$material->name}}</h5>
                                </div>
                                @if($material->type != 5)
                                <div class="col-md-4 text-white pl-3 pr-3">
                                    <div class="row  justify-content-center">
                                        <div class="bg-white">
                                             {!! $code !!}
                                        </div>
                                        
                                        <a href="{{route('materialDownload', ['id' => $material->id, 'site' => $site->id])}}" target="_blank" class="btn btn-form-g mt-5 mb-5 ">Descargar</a>
                                        
                                    </div>
                                </div>
                                @endif
                                <div class="{{($material->type != 5 ? 'col-md-6' : 'col-md-10')}} pl-3 pr-3">
                                    <textarea class="form-control" rows="5" aria-label="With textarea" readonly="" id="banner">{{$code}}</textarea>
                                </div>
                                <div class="col-md-2 pt-3 pr-0 pl-0">
                                    <button type="submit" class="btn btn-form-g pl-5 pr-5" onclick="copy('banner'); return false;">Copiar</button>
                                </div>
                            </div>

                        </form>

                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </section>
        @break
@endswitch
@endforeach