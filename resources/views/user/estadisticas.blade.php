@extends('layout.master')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" >


<style>
    .dataTables_wrapper .dataTables_filter {
       display:none;
    }
    .dataTables_length{
       display:none; 
    }
    
    .dataTables_info{
        display:none; 
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #fff !important;
        border: 1px solid #979797;
        background-color: white;
        background: rgb(47, 74, 159);
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        box-sizing: border-box;
        display: inline-block;
        min-width: 1.5em;
        padding: 0.5em 1em;
        margin-left: 2px;
        text-align: center;
        text-decoration: none !important;
        cursor: pointer;
        *cursor: hand;
        color: #333 !important;
        border: 1px solid transparent;
        border-radius: 2px;
    }
    
    
    div.dataTables_wrapper div.dataTables_paginate {
        margin: 0;
        white-space: nowrap;
        text-align: right;
    }
</style>
@endsection

@section('content')
 <main id="main">
    <section class="bg-white text-center">
      <div class="container">
     <div class="row">
    <div class="col">
     
    </div>
    <div class="col-10">
      <form class="bg-form">
        <div class="row justify-content-md-center">
          <div class="col-md-12 text-center text-white pb-3">
            <h5>Estad&iacute;sticas Avanzandas</h5>
          </div>
        
       <div class="col-md-6">
         <div class="form-group row ">
    <label for="creatividad" class="col-sm-5 col-form-label text-white text-right">Rango:</label>
    <div class="col-sm-7">
      <select class="form-control form-control-lg p-2" id="range">
          <option value=""></option>
          <option value="1">Hoy</option>
          <option value="2">Ayer</option>
          <option value="3" selected="">&Uacute;ltimos 7 d&iacute;as</option>
          <option value="4">Este mes</option>
          <option value="5">Mes pasado</option>
          <option value="6">Este a&ntilde;o</option>
          <option value="7">A&ntilde;o pasado</option>
          <option value="8">Personalizado</option>
    </select>
    </div>
  </div>
       </div>

    <div class="col-md-6">
        <!-- <div class="bg-gradient p-1 text-white">01/03/2020 - 26/003/2020</div> -->
        <input type="text" name="dates" id="dates" class="bg-gradient p-1 text-white" style="width: 100%;text-align: center;border: 0px;" readonly="">
    </div>

    
     </div>
    
  <!-- <button type="submit" class="btn btn-form-g">Guardar</button> -->
</form> 

    </div>
    <div class="col"></div>
        </div>
  </div>
       


  </section>

<div id="result">

</div>
</main>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    <script>
        $(document).ready(function(){
            $('input[name="dates"]').daterangepicker({
                locale: {
                format: 'DD-MM-YYYY'
                },
                maxDate: '{{date('d-m-Y')}}'
            });
            
            function range(){
                var range = $("#range").val();
                $("#dates").prop("readonly", true);
                $("#dates").prop("disabled", true);
                
                 switch(range){
                    case "1":
                      $("#dates").val("{{date('d-m-Y')}} - {{date('d-m-Y')}}");
                    break;
                    case "2":
                      $("#dates").val("{{ date('d-m-Y', strtotime(date('d-m-Y').' -1 day')) }} - {{date('d-m-Y', strtotime(date('d-m-Y').' -1 day'))}}");
                    break;
                    case "3":
                      $("#dates").val("{{ date('d-m-Y', strtotime(date('d-m-Y').' -7 days')) }} - {{date('d-m-Y')}}");
                    break;
                    case "4":
                      $("#dates").val("{{ date('01-m-Y') }} - {{date('d-m-Y')}}");
                    break;
                    case "5":
                        @php
                          $lastMonth = date('m', strtotime('-1 month'));
                          $lastDay = date('t', strtotime('-1 month'));
                        @endphp
                      $("#dates").val("{{ date('01-'.$lastMonth.'-Y')}} - {{date($lastDay.'-'.$lastMonth.'-Y')}}");
                    break;
                    case "6":
                      $("#dates").val("{{ date('01-01-Y') }} - {{date('d-m-Y')}}");
                    break;
                    case "7":
                        @php
                          $lastYear = date('Y', strtotime('-1 year'));
                        @endphp
                      $("#dates").val("{{ date('01-01-'.$lastYear)}} - {{date('31-12-'.$lastYear)}}");
                    break;
                    case "8":
                      $("#dates").val("{{date('01-m-Y')}} - {{date('d-m-Y')}}");
                      $("#dates").prop("disabled", false);
                    break; 
                }
                    
                getData();
            }
            
            function getData(){
                var range = $("#range").val();
                var dates = $("#dates").val();
                
                if(range == "" || dates == ""){
                    return;
                }
                
                $.get("{{route('estadisticasData')}}", {range:range, dates:dates}, function(d){
                    $("#result").html(d);
                })
            }
            
            $("#range").change(function(){ range(); });
            
            range();
            
            $("#dates").change(function(){
                getData();
            });
        });
    </script>
@endsection
