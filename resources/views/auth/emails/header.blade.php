<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel' || true)
<img src="{{route('index')}}/img/logo.png" class="logo" alt="Aficreditos Logo" style="width: 270px; height: 55px;">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
