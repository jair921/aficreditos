<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Aficreditos</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('img/favicon.png')}}" rel="icon">
        <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendor/icofont/icofont.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <style>
            .error{
                color: #fff;
            }
        </style>
    </head>

    <body>
        <header id="header" class="bg-white">
            <div class="container d-flex ">
                <div class="logo">
                    <a href="{{route('index')}}"><img src="{{asset('img/logo.png')}}" alt="" style="margin: 10px;"></a>
                </div>
                <nav class="nav-menu navbar justify-content-md-center d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="#main">COMO FUNCIONA</a></li>
                        <li><a href="#comisiones">COMISIONES</a></li>
                        <li><a href="#registro">REGISTRO</a></li>
                        <li><a href="#footer">CONTACTO</a></li>
                        <li><div class="card form text-center">
                                <div class="card-body">
                                    <p class="card-title text-white" style="font-size: 15px;">Ingreso afiliados</p>
                                     @guest
                                    <form class="pr-2 pl-2" method="post" action="{{route('login')}}">
                                        @csrf
                                        <div class="form-group ">
                                            <input type="text" name="username" class="form-control" style="padding: 1rem 1rem;" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Usuario" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password"  name="password" class="form-control" style="padding: 1rem 1rem;" id="exampleInputPassword1" placeholder="Clave" required="">
                                        </div>
                                        <button type="submit" class="btn btn-form pl-5 pr-5 pt-1 pb-1">Ingresar</button>
                                    </form>
                                    @endguest
                                    @auth
                                      <a href="{{route('home')}}" class="btn" style="background: #2F4A9F; border-radius: 30px;  color: #fff !important;">Mi cuenta</a>
                                    @endauth
                                </div>
                            </div>
                        </li>

                    </ul>

                </nav>

            </div>
        </header>

        <section id="hero" class="hero">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-md-6 py-5 py-lg-0 order-2 order-lg-1" >
                        <h2>Registrate hoy mismo <strong style="color: #2F4A9F;">haciendo <a href="#registro">click aquí</a></strong> y empezá a rentabilizar tu audiencia.</h2>
                    </div>
                </div>
            </div>

        </section>

        <main id="main">
            <section class="bg-white">
                <div class="container">

                    <div class="section-title pb-5">
                        <h2 >Somos el mayor  programa de afiliados financiero del país</h2>
                        <p></p>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-5">
                            <div class="card">
                                <img src="{{asset('img/herramienta.png')}}" class="img-fluid position-absolute">
                                <div class="card-body">
                                    <h3 class="card-title text-right"> Herramientas innovadoras </h3>
                                    <p class="card-text" style="color: #333333; font-size: 20px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-5">
                            <div class="card">
                                <img src="{{asset('img/estadisticas.png')}}" class="img-fluid position-absolute">
                                <div class="card-body">
                                    <h3 class="card-title text-right">Estadisticas transparentes</h3>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-5">
                            <div class="card">
                                <img src="{{asset('img/comisiones.png')}}" class="img-fluid position-absolute">
                                <div class="card-body">
                                    <h3 class="card-title text-right">Altas comisiones</h3>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-5">
                            <div class="card">
                                <img src="{{asset('img/pagos.png')}}" class="img-fluid position-absolute">
                                <div class="card-body">
                                    <h3 class="card-title text-right" >Pagos puntuales</h3>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="section-title-funciona">
                        <h2 style="font-size: 38px" >¿Como funciona?</h2 >
                        <p>Si tenes una pagina web o sos influencer de alguna red social, podes monetizar tu tráfico con nosotros!  te brindamos las herramientas necesarias para hacerlo, es simple! Al registrarte en nuestro programa de afiliados te vamos a dar un link único, junto con creaticidades (banners, imagenes publicitarias, etc...) para que puedas promocionar nuestras marcas. Por cada cliente que obtenga un prestamo con nosotros (brindamos prestamos sin requisitos!), obtenes una comisión basada en el importe bruto del prestamo, todo esto lo vas a poder ver en un panel de control con estadisticas detalladas y totalmente transparentes. Monetizar tu tráfico nunca fue tan fácil!</p>
                    </div>
                </div>

                </div>
            </section>
            <section id="comisiones" class="comisiones pt-5 pb-0">
                <div class="container">
                    <div class="section-title pb-0">
                        <h2 class="text-white">Comisiones</h2>

                    </div>
                    <div class="row">
                        <div class="image col-xl-5 align-items-stretch justify-content-center justify-content-lg-start mb-5">
                        </div>

                        <div class="col-xl-7 d-flex align-items-stretch" style="padding-top: 5rem; padding-bottom: 5rem;">
                            <div class="content flex-column justify-content-center">
                                <p style="font-size: 25px;">
                                    Obtene un 10% de comisión sobre los prestamos que le otorgamos a tus clientes! Porque sabemos que sos nuestro socio estategico, te brindamos la comisión más alta de la industria.
                                </p>

                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <section id="registro" class="registro bg-white">

                <div class="container">
                    <div class="row">
                        <div class="col-md-6 pt-5 texto">

                            <h3 style="font-size: 48px; color: #2F4A9F;">Completa el siguiente formulario y <span style="color: #EC602B"><strong>empezá a rentabilizar</strong></span> tu tráfico hoy mismo!</h3>

                        </div>

                        <div class="col-md-6 formulario">
                            <form action="{{route('register')}}" method="post" role="form" class="php-email-form" id="frm-register">
                                @csrf
                                <img src="{{asset('img/registro.png')}}" class="img-fluid pt-3 pb-3">
                                <div class=" form-group">
                                    <input type="text" onkeyup="validUsername()" onblur="validUsername()" name="username" value="{{old('username')}}" class="form-control" id="username" placeholder="Nombre de usuario" data-rule="minlen:4" required="">
                                    <span id="msgUsername" style="color: #fff;"></span>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" data-rule="minlen:4"  required="">

                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" value="{{old('username')}}" id="name" placeholder="Nombre y apellido" data-rule="minlen:4" required="">

                                </div>
                                <div class=" form-group">
                                    <input type="date" name="date_birth" value="{{old('date_birth')}}" class="form-control" id="date_birt" placeholder="Fecha de nacimiento" data-rule="minlen:4"  required="">

                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cuit" value="{{old('cuit')}}" id="cuit" placeholder="Cuit/Cuil" data-rule="email" required="" >

                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}" id="address" placeholder="Domicilio" data-rule="minlen:4" required="" >

                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" value="{{old('phone')}}" id="phone" placeholder="Telefono" data-rule="minlen:4" required="">

                                </div>
                                <div class="form-group">
                                    <input type="email" onkeyup="validEmail()" onblur="validEmail()" class="form-control" name="email" value="{{old('email')}}" id="email" placeholder="Email" data-rule="email" required="" >
                                    <span id="msgEmail" style="color: #fff;"></span>
                                </div>
                                <div class=" text-center pt-3 pb-3"><button type="submit">REGISTRARSE</button></div>
                            </form>
                        </div>

                    </div>

                </div>

            </section>

        </main>

        <footer id="footer" >

            <div class="footer-top">

                <div class="container">
                    <div class="section-title">
                        <h2 class="text-white">Contacto</h2>
                        <p class="text-white">Tenés alguna duda? envianos tu mensaje y te contactaremos a la brevedad</p>
                    </div>
                    <div class="row  justify-content-center">
                        <div class="col-md-12">

                            <form method="post" action="{{route('contacto')}}">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Nombre y Apellido" name="name" required="">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Telefono" name="phone" required="">
                                    </div>
                                    <div class="col">
                                        <input type="email" class="form-control" placeholder="Email" name="email" required="">
                                    </div>

                                </div>

                                <div class="form-group pt-2" >

                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Mensaje" name="message" required=""></textarea>
                                </div>

                                <button type="submit" class="btn btn-form-g">ENVIAR MENSAJE</button>
                            </form>
                        </div>

                    </div>




                </div>
            </div>

            <div class="container footer-bottom clearfix bg-white">
                <div class="row">
                    <div class="col enlaces">
                        <a href="#">Enlace</a>  <span><a href="#">Enlace</a></span> <a href="#">Enlace</a>
                    </div>
                    <div class="col copyright">
                        <p>Copyright © {{date('Y')}}</p>
                    </div>
                    <div class="col creditos">
                        <a href="{{route('index')}}">Aficreditos.com</a>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{asset('vendor/php-email-form/validate.js')}}"></script>
        <script src="{{asset('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js "></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/localization/messages_es.js"></script>
        <script src="{{asset('js/main.js')}}"></script>

        @if(Session::has('message'))
        <script>
            $(document).ready(function () {
              toastr.{{Session::get('alert-type', 'info')}}('{!! Session::get("message") !!}');
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "5000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              };
            });
        </script>
        @endif
        @if($errors->any())
        <script>
            $(document).ready(function () {
            toastr.error('<ul>{!! implode("", $errors->all("<li>:message</li>")) !!}</ul>');
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "15000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              };
            });
        </script>
        
        @endif
        
        <script>
            var submit = true;
            function validUsername(){
                var username = $("#username").val();
                submit = false;
                $.get("{{route('valid')}}", {username:username}, function(d){
                    if(d=="true"){
                        submit = true;
                        $("#msgUsername").text("");
                    }else{
                        submit = false;
                        $("#msgUsername").text("El usuario ya se encuentra registrado.");
                    }
                });
            }
            
            function validEmail(){
                var email = $("#email").val();
                submit = false;
                $.get("{{route('valid')}}", {email:email}, function(d){
                    if(d=="true"){
                        submit = true;
                        $("#msgEmail").text("");
                    }else{
                        submit = false;
                        $("#msgEmail").text("El Email ya se encuentra registrado.");
                    }
                });
            }
            $(document).ready(function(){
                $("#frm-register").submit(function(e){
                    
                    if(!submit){
                        e.preventDefault();
                    }
                    
                });
            });
        </script>
        <script>
            var csrfToken = $('[name="csrf_token"]').attr('content');

            //setInterval(refreshToken, 900000); // 1 hour 

            function refreshToken(){
                $.get('refresh-csrf').done(function(data){
                    csrfToken = data; // the new token
                    $('[name="csrf_token"]').attr('content', csrfToken);
                    $('input[name="_token"]').each(function(){ $(this).attr('value', csrfToken); });
                });
            }

            setInterval(refreshToken, 900000);

        </script>

    </body>

</html>
