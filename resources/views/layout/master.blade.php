<!DOCTYPE html>
<html lang="en">

    <head><meta charset="gb18030">
        
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Aficreditos</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('img/favicon.png')}}" rel="icon">
        <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendor/icofont/icofont.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        @yield('css')
    </head>

    <body class="d-flex flex-column h-100" style="overflow-x: hidden;">
        <header id="header" class="bg-white border-bottom">
            <div class="container d-flex ">
                <div class="logo">
                    <a href="{{route('home')}}"><img src="{{asset('img/logo.png')}}" alt="" ></a>
                </div>
                <nav class="nav-menu navbar justify-content-md-center d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('material')}}">Material de Marketing </a></li>
                        <li><a href="{{route('estadisticas')}}">Estad&iacute;sticas</a></li>
                        <li><a href="{{route('cajero')}}">Cajero</a></li>
                        <li><a href="{{route('myaccount')}}">Mi cuenta</a></li>
                        <li><a href="{{route('contactoAfi')}}">Contacto</a></li>
                        <li><a href="{{route('app.logout')}}">Logout</a></li>
                        <li>
                            <div class="card-white position-absolute">
                                <div class="row no-gutters">
                                    <div class="col-5 border pl-4 pr-4 pt-3 pb-3 align-middle">
                                        <p class="w-100 text-naranja p-0 m-0">Bienvenido: {{auth()->user()->name}}!</p>
                                        <p class="p-0 m-0 ">Comisi&oacute;n: <span class="text-azul">${{number_format(\App\Withdrawal::cash(), 2)}}</span></p>
                                    </div>

                                    <div class="col-md-4 p-2 align-middle">
                                        <span class="badge badge-pill badge-danger notify-badge position-absolute">0</span>
                                        <img src="{{asset('img/avatar.png')}}" class="img-fluid" alt="avatar">
                                    </div>
                                </div>
                            </div>


                        </li>

                    </ul>

                </nav>

            </div>
        </header>

        @yield('content')

        <footer class="footer mt-auto py-3 static-bottom">
            <div class="container">
                <div class="row">
                    <div class="col enlaces">
                        <a href="#">Enlace</a>  <span><a href="#">Enlace</a></span> <a href="#">Enlace</a> 
                    </div>
                    <div class="col copyright">
                        <p>Copyright © 2020</p>
                    </div>
                    <div class="col creditos">
                        <a href="{{route('index')}}">Aficreditos.com</a>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
        
        <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{asset('vendor/php-email-form/validate.js')}}"></script>
        <script src="{{asset('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js "></script>
        <script src="{{asset('js/main.js')}}"></script>

        <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        @yield('js')
         @if(Session::has('message'))
        <script>
            $(document).ready(function () {
              toastr.{{Session::get('alert-type', 'info')}}('{!! Session::get("message") !!}');
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "5000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              };
            });
        </script>
        @endif
        @if($errors->any())
        <script>
            $(document).ready(function () {
            toastr.error('<ul>{!! implode("", $errors->all("<li>:message</li>")) !!}</ul>');
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "5000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              };
            });
        </script>
        @endif
        <script>
            var csrfToken = $('[name="csrf_token"]').attr('content');

            //setInterval(refreshToken, 900000); // 1 hour 

            function refreshToken(){
                $.get('refresh-csrf').done(function(data){
                    csrfToken = data; // the new token
                    $('[name="csrf_token"]').attr('content', csrfToken);
                    $('input[name="_token"]').each(function(){ $(this).attr('value', csrfToken); });
                });
            }

            setInterval(refreshToken, 900000);

        </script>
    </body>


</html>
