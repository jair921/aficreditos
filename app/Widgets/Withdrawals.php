<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class Withdrawals extends \TCG\Voyager\Widgets\BaseDimmer {

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run() {

       $withdrawal = \App\Withdrawal::where('status', 1)->count();
        
        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-dollar',
            'title' => "$withdrawal Solicitudes de retiro pendiente",
            'text' => "",
            'button' => [
                'text' => 'Ver ' ,
                'link' => route('voyager.withdrawals.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/02.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed() {
        
        return true;
    }

}
