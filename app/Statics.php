<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statics extends Model
{
    public $table = 'statics';
    
    public function user(){
        return $this->belongsTo('\App\User');
    }
}
