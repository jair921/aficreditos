<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Withdrawal extends Model
{
    
    
    public static function cash(){
        
        $commission = \App\Statics::where('user_id', auth()->id())
                    ->selectRaw(" sum(mount * (commission/100)) as cash ")
                   ->where('type', 3)
                   ->first();
        $Withdrawal =\App\Withdrawal::where('user_id', auth()->id())
                      
                      ->sum("amount");
                      
                   
        return ($commission->cash - $Withdrawal);
    }
    
    public static function cashReal(){
        
        $commission = \App\Statics::where('user_id', auth()->id())
                    ->selectRaw(" sum(mount * (commission/100)) as cash ")
                   ->where('type', 3)
                   ->first();
        $Withdrawal =\App\Withdrawal::where('user_id', auth()->id())
                      ->where('status', 2)
                      ->sum("amount");
                      
                   
        return ($commission->cash - $Withdrawal);
    }    
}
