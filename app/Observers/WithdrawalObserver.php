<?php

namespace App\Observers;

use App\Withdrawal;

class WithdrawalObserver
{
    /**
     * Handle the withdrawal "created" event.
     *
     * @param  \App\Withdrawal  $withdrawal
     * @return void
     */
    public function created(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Handle the withdrawal "updated" event.
     *
     * @param  \App\Withdrawal  $withdrawal
     * @return void
     */
    public function updated(Withdrawal $withdrawal)
    {
        if($withdrawal->status=='2'){
          //  \App\Statics::where('user_id', $withdrawal->user_id)->where('type', 3)->where('withdrawal', 1)->update(['withdrawal' => 2]);
        }
        
        $amount = \App\Withdrawal::where('status', 2)->where('user_id', $withdrawal->user_id)->sum('amount');
        $level = \App\Level::where('end', '>=', $amount)->where('start', '<=', $amount)->first();
        $user = \App\User::find($withdrawal->user_id);
        if($level && $user && $user->level_id != $level->id){
            $user->level_id = $level->id;
            $user->save();
        }
            
    }

    /**
     * Handle the withdrawal "deleted" event.
     *
     * @param  \App\Withdrawal  $withdrawal
     * @return void
     */
    public function deleted(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Handle the withdrawal "restored" event.
     *
     * @param  \App\Withdrawal  $withdrawal
     * @return void
     */
    public function restored(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Handle the withdrawal "force deleted" event.
     *
     * @param  \App\Withdrawal  $withdrawal
     * @return void
     */
    public function forceDeleted(Withdrawal $withdrawal)
    {
        //
    }
}
