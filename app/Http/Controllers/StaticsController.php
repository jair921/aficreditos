<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticsController extends Controller
{
    public function index(Request $request){
        
        //dd($request);
        
        if($request->pay == '0'){
            $statics = new \App\Statics();
            $statics->type=1;
            $statics->user_id = $request->referer;
            $statics->track = $request->track;
            $statics->request = $request->request_id;
            $statics->site = $request->site;
            $statics->save();    
        }
 
        if($request->pay == '0' && $request->request_id){
            $statics = new \App\Statics();
            $statics->type=2;
            $statics->user_id = $request->referer;
            $statics->track = $request->track;
            $statics->request = $request->request_id;
            $statics->site = $request->site;
            $statics->save();    
        }
        
        if($request->pay != '0'){
            
            $commission=10;
            $user = \App\User::find($request->referer);
            if($user){
               $commission=$user->commission;    
            }
            
            $statics = new \App\Statics();
            $statics->type=3;
            $statics->user_id = $request->referer;
            $statics->track = $request->track;
            $statics->request = $request->request_id;
            $statics->site = $request->site;
            $statics->mount = $request->mount;
            $statics->name = $request->name;
            $statics->commission = $commission;
            $statics->save();    
        }
        
        return ['ok'];
       
    }
}
