<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $dateStart = date('Y-m-d 00:00:00');
        $dateEnd = date('Y-m-d 23:59:59');
        
        $clicks = \App\Statics::where('user_id', auth()->id())->where('type', 1)->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
        $prints = \App\Statics::where('user_id', auth()->id())->where('type', 4)->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
        $registers = \App\Statics::where('user_id', auth()->id())->where('type', 2)->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
        $pays = \App\Statics::where('user_id', auth()->id())->where('type', 3)->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->count();
        
        $lstPayToday = \App\Statics::selectRaw("mount*(commission/100) as amount")->where('user_id', auth()->id())->where('type', 3)->where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
        $payToday=0;
        foreach($lstPayToday as $p){
            $payToday += $p->amount;
        }
//        dd($payToday);
        
        $lstPayMonth = \App\Statics::selectRaw("mount*(commission/100) as amount")->where('user_id', auth()->id())->where('type', 3)->where('created_at', '>=', date('Y-m-01 00:00:00'))
                      ->where('created_at', '<=', date('Y-m-t 23:59:59'))->get();
        $payMonth = 0;   
        foreach($lstPayMonth as $p2){
          $payMonth += $p2->amount;
        }
        
        return view('user.index', compact('clicks', 'registers', 'pays', 'payToday', 'payMonth', 'prints'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function mydata()
    {
        return view('user.mydata');
    }
    
    public function changePass(Request $request){
        
        $request->validate([
            'password' => ['required', 'string'],
            'newpassword' => ['required', 'string', 'min:4', 'confirmed'],
        ]);
        
        $user = auth()->user();

        if (Hash::check(Hash::make($request->password), $user->password)){
            return redirect()->route('myaccount')->with([
                'message' => 'La contraseña actual no coincide.',
                'alert-type' => 'warning'
            ]);
        }
        
        $user->password = Hash::make($request->newpassword);
        
        $user->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('myaccount')->with([
                        'message' => 'Contraseña actualizada correctamente',
                        'alert-type' => 'success'
                    ]);
        
    }
    
    public function changeData(Request $request){
        $request->validate([
            'name' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'address' => ['required', 'string'],
            'provincia' => ['required'],
        ]);
        
        $user = auth()->user();
        
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->provincia = $request->provincia;
        
        $user->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('myaccount')->with([
                        'message' => 'Información actualizada correctamente',
                        'alert-type' => 'success'
                    ]);
    }
    
    public function cajero(Request $request){
        $min = setting('site.withdrawal.mount')['withdrawal'];
        
        //$pay = \App\Statics::where('user_id', auth()->id())->where('type', 3)->whereNull('withdrawal')->sum('mount');
        
        $realPay = \App\Withdrawal::cash();
        
        //$inProccess = \App\Withdrawal::where('status', 1)->where('user_id', auth()->id())->count();
        $withdrawals = \App\Withdrawal::where('user_id', auth()->id())->orderBy('id', 'desc')->paginate(15);
        
        $ds = ($realPay < (float)$min); // || $inProccess > 0;
        
        return view('user.cajero', compact('min', 'realPay', 'ds', 'withdrawals'));
    }
    
    public function changeDataBank(Request $request){
        $request->validate([
            'name' => ['required'],
            'cuit' => ['required'],
            'bank' => ['required'],
            'type' => ['required'],
            'cbu' => ['required'],
        ]);
        
        $user = auth()->user();
        
        $user->name = $request->name;
        $user->cuit = $request->cuit;
        $user->bank = $request->bank;
        $user->type = $request->type;
        $user->cbu = $request->cbu;
        
        $user->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('cajero')->with([
                        'message' => 'Información actualizada correctamente',
                        'alert-type' => 'success'
                    ]);
    }
    
    public function retiro(Request $request){
        
        $min = setting('site.withdrawal.mount')['withdrawal'];
        
        $realPay = \App\Withdrawal::cash();
        
        $validatedData = $request->validate([
            'amount' => 'required|numeric|max:'.$realPay.'|min:'.$min,
        ]);
        
        $user = auth()->user();

        $withdrawal = new \App\Withdrawal();
        $withdrawal->user_id = $user->id;
        $withdrawal->cbu = $user->cbu;
        $withdrawal->status = 1;
        $withdrawal->amount = $request->amount;
        $withdrawal->commission = auth()->user()->commission;
        $withdrawal->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('cajero')->with([
                        'message' => 'Retiro solicitado',
                        'alert-type' => 'success'
                    ]);
    }
    
    public function contacto(Request $request){
        
        return view('user.contacto');
    }
    
    public function contactoPost(Request $request){
        
        $request->validate([
            'asunto' => ['required'],
            'mensaje' => ['required'],
        ]);
        
        $user = auth()->user();
        
        $contact = new \App\Contact();
        
        $contact->user_id = $user->id;
        $contact->asunto = $request->asunto;
        $contact->mensaje = $request->mensaje;
        
        $contact->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('contactoAfi')->with([
                        'message' => 'Solicitud de contacto enviada.',
                        'alert-type' => 'success'
                    ]);
    }
    
    public function contactoUser(Request $request){
        
        $request->validate([
            'name' => ['required'],
            'phone' => ['required'],
            'email' => ['required'],
            'message' => ['required'],
        ]);
        
        $contact = new \App\ContactUser();
        
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->message = $request->message;
        
        $contact->save();
        
        return $request->wantsJson()
                    ? new Response('OK', 201)
                    : redirect()->route('index')->with([
                        'message' => 'Solicitud de contacto enviada.',
                        'alert-type' => 'success'
                    ]);
    }

    public function material(Request $request){
        
        return view('user.material');
    }
    
    public function materialData($id, $siteID){
        
        $site = \App\Site::find($siteID);
        $materials = \App\Material::where('status', 1)->where('type', $id)->where('site_id', $site->id)->get();

        return view('user.materialData', compact('materials', 'site'));
    }
    
    
    public function materialDownload($id, $siteID){
        
        $material = \App\Material::find($id);
        $site = \App\Site::find($siteID);
        

        if($material->type = 2 || $material->type = 3){
            
            list($ab, $file) = explode('__user__', $material->code);
            list($ac, $extension) = explode('.', $file);
            
            $path = "storage$file";
            
            header("Content-Type: ".\Illuminate\Http\Testing\MimeType::from($path));
            header("Cache-Control: no-store,no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0 ");
            header("Pragma: no-cache");
            header("expires: Jue, 31 Oct 2017 18:00:00 GMT");
            header("Powered-By: Aficreditos");
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' .($material->type = 2 ? 'banner' : 'video').'.'.$extension );
            header('Content-Length: ' . filesize($path));
            $f = fopen($path, 'r');
            while(!feof($f)){
                echo fgets($f, 1024*1024);
                flush();
            }
            fclose($f);
            
            
            return;
        }
        
        $link = $site->url.'/ref/?cff='.auth()->user()->id.'';
        
        $html = str_replace('LINKUSER', $link, $material->code);
        $html = str_replace('__site__', $material->site_id, $html);
        $html = str_replace('__user__', auth()->id(), $html);
        
        $headers = array(
              'Content-Type: text/html',
            );
        $response = Response::make($html, 200);
        $response->header('Content-Type', 'application/html');
        $response->header('Content-Disposition', 'attachment; filename="'.$material->name.'.html"');
        return $response;
    }
    
    public function track(Request $request, $siteID, $usr, $folder, $file){
        
        /*try{
          $statics = new \App\Statics();
          $statics->type=4;
          $statics->user_id = $usr;
          $statics->site = \App\Site::find($siteID)->url;
          $statics->save();
        }catch(Exception $e){
        }*/
        
        $path = "storage/$folder/$file";
        
        
        header("Cache-Control: no-store,no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0 ");
        header("pragma: no-cache");
        header("expires: Jue, 31 Oct 2017 18:00:00 GMT");
        header("Powered-By: Aficreditos");
        header("Last-Modified: ".gmdate("D, 21 09 1992 H:i:s")." GMT");
        header("afi: 4");
        
        flush();
        
        header("Content-Type: ".\Illuminate\Http\Testing\MimeType::from($path));
        
        $f = fopen($path, 'r');
        while(!feof($f)){
            echo fgets($f, 1024);
            flush();
        }
        fclose($f);
        
        die();
       
        $headers =[
            'Content-Type' => \Illuminate\Http\Testing\MimeType::from($path),
            'Cache-Control' => 'no-store,no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0 ',
            'Pragma' => 'no-cache',
            'Powered-By' => 'Aficreditos',
         ];
        
        return response()->file($path, $headers);
    }
    
    public function tracking(Request $request, $siteID, $usr, $folder, $file){
        
        try{
          $statics = new \App\Statics();
          $statics->type=4;
          $statics->user_id = $usr;
          $statics->site = \App\Site::find($siteID)->url;
          $statics->save();
        }catch(Exception $e){
        }
        
        echo "tracking....";
    }
    
    public function siteMaterials($site){
        $materials = \App\Material::where('status', 1)->where('site_id', $site)->get();
        
        foreach($materials as $material){
            echo '<option value="'.$material->id.'">'.$material->name.'</option>';
        }
        
    }
    
    public function estadisticas(){
        return view('user.estadisticas');
    }
    
    public function estadisticasData(Request $request){
        
        list($dateStart, $dateEnd) = explode(" - ", $request->dates);

        $range = $request->range;
        $di='';
        switch($range){
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
                $di = "P1D";
                break;
            case "6":
            case "7":
                $di = "P1M";
                break;
            case "9":
                $di = "P1Y";
                break;                
        }

        return view('user.estadisticasData', compact('dateStart', 'dateEnd', 'range', 'di'));
    }   
    
    
    public function estadisticasDataAdmin(Request $request){
        
        list($dateStart, $dateEnd) = explode(" - ", $request->dates);
        $userId = $request->user;
        $range = $request->range;
        $di='';
        switch($range){
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
                $di = "P1D";
                break;
            case "6":
            case "7":
                $di = "P1M";
                break;
            case "9":
                $di = "P1Y";
                break;                
        }
        
        $view = "estadisticasAdminDataAll";
        
        if($userId){
            $view = 'estadisticasAdminData';
        }

        return view('user.'.$view, compact('dateStart', 'dateEnd', 'range', 'di', 'userId'));
    }   
}
