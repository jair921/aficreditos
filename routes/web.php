<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

/*Route::get('/login8', function () {
    $user = \App\User::find(8);
    Auth::login($user);
    return redirect()->route("home");
})->name('logiii');*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    
    Route::get('/estadisticas/data', 'HomeController@estadisticasDataAdmin')->name('estadisticasData.admin');
});

Route::group(['prefix' => 'home', 'middleware' => ['auth', 'verified']], function () {
    
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/mi-cuenta', 'HomeController@mydata')->name('myaccount');
    Route::post('/changePass', 'HomeController@changePass')->name('changePass');
    Route::post('/changeData', 'HomeController@changeData')->name('changeData');
    
    Route::get('/cajero', 'HomeController@cajero')->name('cajero');
    Route::post('/cajero', 'HomeController@changeDataBank')->name('changeDataBank');
    
    Route::post('/retiro', 'HomeController@retiro')->name('retiro');
    Route::get('/contacto', 'HomeController@contacto')->name('contactoAfi');
    Route::post('/contactoP', 'HomeController@contactoPost')->name('contactoPost');

    Route::get('/material', 'HomeController@material')->name('material');
    Route::get('/material/site/{id}', 'HomeController@siteMaterials')->name('siteMaterials');
    Route::get('/material/{id}/{site}', 'HomeController@materialData')->name('materialData');
    Route::get('/material/download/{id}/{site}', 'HomeController@materialDownload')->name('materialDownload');
    
    Route::get('/estadisticas', 'HomeController@estadisticas')->name('estadisticas');
    Route::get('/estadisticas/data', 'HomeController@estadisticasData')->name('estadisticasData');
});

Route::post('/contactohome', 'HomeController@contactoUser')->name('contacto');
Route::get('/track/{site}/{usr}/{folder}/{file}', 'HomeController@track')->name('file.track');
Route::get('/tracking/{site}/{usr}/{folder}/{file}', 'HomeController@tracking')->name('file.tracking');

Auth::routes(['verify' => true]);

Route::get('logout', function(){
        Auth::logout();
        return redirect()->route('index');
    })->name('app.logout');


//Auth::routes();
Route::get('/register', function(){ return redirect()->route('index'); })->name('register');
Route::get('/login', function(){ return redirect()->route('index'); })->name('login');

Route::post('/statics', 'StaticsController@index')->name('statics');
Route::get('/valid', function(\Illuminate\Http\Request $request){

    if(isset($request->username)){
        $cu = \App\User::where('username', $request->username)->count();
        if($cu>0){
            echo "false";
            return;
        }
    }
    
    if(isset($request->email)){
        $ce = \App\User::where('email', $request->email)->count();
        if($ce>0){
            echo "false";
            return;
        }
    }    
    
    echo "true";
})->name('valid');

Route::get('refresh-csrf', function(){
    return csrf_token();
});

